import 'package:persist_with_sqlite/cat_dao.dart';
import 'cat.dart';
import 'dog.dart';
import 'dog_dao.dart';

void main() async {
  var fido = Dog(id: 0, name: 'Fido', age: 35);
  var dido = Dog(id: 1, name: 'Dido', age: 20);
  await DogDao.insertDog(fido);
  await DogDao.insertDog(dido);

  print(await DogDao.dogs());

  fido = Dog(id: fido.id, name: fido.name, age: fido.age + 7);
  await DogDao.updateDog(fido);
  print(await DogDao.dogs());

  await DogDao.deleteDog(0);
  print(await DogDao.dogs());

  var lucas = Cat(id: 0, name: 'Lucas', age: 3);
  var cactus = Cat(id: 1, name: 'Cactus', age: 5);
  await CatDao.insertCat(lucas);
  await CatDao.insertCat(cactus);

  print(await CatDao.cats());

  lucas = Cat(id: lucas.id, name: lucas.name, age: lucas.age - 2);
  cactus = Cat(id: cactus.id, name: 'cactux', age: cactus.age);
  await CatDao.updateCat(lucas);
  await CatDao.updateCat(cactus);

  await CatDao.deleteCat(0);
  print(await CatDao.cats());
}
