import 'package:persist_with_sqlite/database_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'cat.dart';

class CatDao {
  static Future<void> insertCat(Cat cat) async {
    final db = await DatabaseProvider.database;
    await db.insert('cats', cat.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  static Future<List<Cat>> cats() async {
    final db = await DatabaseProvider.database;

    return Cat.toList(
      await db.query('cats'),
    );
  }

  static Future<void> updateCat(Cat cat) async {
    final db = await DatabaseProvider.database;

    await db.update(
      'cats',
      cat.toMap(),
      where: 'id = ?',
      whereArgs: [cat.id],
    );
  }

  static Future<void> deleteCat(int id) async {
    final db = await DatabaseProvider.database;
    await db.delete('cats', where: 'id = ?', whereArgs: [id]);
  }
}
